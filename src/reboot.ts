import {reboot} from './reboot-cmd';

function main() {
  const nodesToReboot = process.argv.slice(2);
  reboot(nodesToReboot);
}

main();
