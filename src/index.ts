#!/bin/env node

import {Listr} from 'listr2';
import {inspect} from 'util';
import {regions} from './config';
import {destroy} from './destroy-cmd';
import {
  stacksByRegion2instancesByNetwork,
  stacksByRegion,
  Ctx,
  populateStacksByRegion,
  getInstanceIdsForStacks,
  getInstanceIps,
} from './utils';

async function main() {
  if (process.argv[2] === 'destroy') {
    if (!process.argv[3]) {
      throw 'No network given to destroy';
    }
    destroy(process.argv[3]);
  } else {
    listNetworks();
  }
  return;
}

async function listNetworks(): Promise<void> {
  const mainTasks = new Listr<Ctx>(
    [
      {
        title: 'Getting PROD/DEV net CloudFormation Stacks by region',
        task: (_ctx, task): Listr => {
          const tasks = Object.keys(regions).map(region => {
            return {
              title: region,
              task: () => populateStacksByRegion(region),
            };
          });

          return task.newListr(tasks, {
            concurrent: true,
          });
        },
      },
      {
        title: 'Getting EC2 instances allocated to each Stack',
        task: (ctx, task): Listr => {
          const regionTasks = Object.keys(stacksByRegion).map(region => {
            return {
              title: region,
              task: () =>
                Promise.all(
                  Object.keys(stacksByRegion[region]).map(stack => {
                    return getInstanceIdsForStacks(region, stack);
                  })
                ),
            };
          });
          return task.newListr(regionTasks, {concurrent: false});
        },
      },
      {
        title: 'Getting EC2 instance IPs',
        task: (ctx, task): Listr => {
          const regionTasks = Object.keys(stacksByRegion).map(region => {
            return {
              title: region,
              task: () => getInstanceIps(region),
            };
          });
          return task.newListr(regionTasks, {concurrent: false});
        },
      },
    ],
    {rendererOptions: {collapse: false}}
  );

  try {
    await mainTasks.run();

    // Rearrange stacksByRegion into instancesByNetwork
    const instancesByNetwork =
      stacksByRegion2instancesByNetwork(stacksByRegion);

    if (process.argv[2] === '--json') {
      console.log(JSON.stringify(instancesByNetwork));
      console.log(JSON.stringify(stacksByRegion));
    } else {
      console.log();
      console.log('============================');
      console.log('=== INSTANCES BY NETWORK ===');
      console.log('============================');
      console.log();
      console.log(inspect(instancesByNetwork, false, null, true));

      console.log();
      console.log('========================');
      console.log('=== STACKS BY REGION ===');
      console.log('========================');
      console.log();
      console.log(inspect(stacksByRegion, false, null, true));
    }
  } catch (e: unknown) {
    console.error(e);
  }
}

main();
