import {Listr} from 'listr2';
import {regions} from './config';
import {
  deleteStack,
  stacksByRegion2instancesByNetwork,
  Ctx,
  populateStacksByRegion,
  stacksByRegion,
} from './utils';

export async function deleteNetworkStacksInRegion(
  task: any,
  regionName: string,
  region: {
    [stack: string]: string[];
  }
) {
  const deleteCmds = Object.keys(region).map(stack => {
    task.output = `${regionName}, ${stack}`;
    return deleteStack(regionName, stack);
  });
  await Promise.all(deleteCmds);
}

export async function destroy(networkToDestroy: string): Promise<void> {
  try {
    // Get PROD/DEV network stacks
    const mainTasks = new Listr<Ctx>(
      [
        {
          title: 'Getting PROD/DEV net CloudFormation Stacks by region',
          task: (_ctx, task): Listr => {
            const tasks = Object.keys(regions).map(region => {
              return {
                title: region,
                task: () => populateStacksByRegion(region),
              };
            });

            return task.newListr(tasks, {
              concurrent: true,
            });
          },
        },
      ],
      {rendererOptions: {collapse: false}}
    );
    await mainTasks.run();

    // Rearrange stacksByRegion into instancesByNetwork
    const instancesByNetwork =
      stacksByRegion2instancesByNetwork(stacksByRegion);

    /*
    const instancesByNetwork: {
      [network: string]: {[region: string]: {[stack: string]: string[]}};
    } = {
      PROD: {},
      'PROD-2': {},
      DEV: {},
    };

    for (const region in stacksByRegion) {
      for (const stack in stacksByRegion[region]) {
        switch (true) {
          case stack.includes('PROD-2'): {
            if (!instancesByNetwork['PROD-2'][region])
              instancesByNetwork['PROD-2'][region] = {};
            instancesByNetwork['PROD-2'][region][stack] =
              stacksByRegion[region][stack];
            break;
          }
          case stack.includes('DEV'): {
            if (!instancesByNetwork['DEV'][region])
              instancesByNetwork['DEV'][region] = {};
            instancesByNetwork['DEV'][region][stack] =
              stacksByRegion[region][stack];
            break;
          }
          default:
            if (!instancesByNetwork['PROD'][region])
              instancesByNetwork['PROD'][region] = {};
            instancesByNetwork['PROD'][region][stack] =
              stacksByRegion[region][stack];
            break;
        }
      }
    }
    */

    // Check if network is already down
    const network = instancesByNetwork[networkToDestroy];
    if (!network) {
      console.log('Network is not up');
      // eslint-disable-next-line no-process-exit
      process.exit();
    }

    // Delete stacks for the given Network
    const deleteTasks = new Listr<Ctx>(
      [
        {
          title: `Deleting stacks for ${networkToDestroy} network`,
          task: (_ctx, task): Listr => {
            const tasks = Object.keys(instancesByNetwork[networkToDestroy]).map(
              region => {
                return {
                  title: region,
                  task: () =>
                    deleteNetworkStacksInRegion(
                      task,
                      region,
                      instancesByNetwork[networkToDestroy][region]
                    ),
                };
              }
            );

            return task.newListr(tasks, {
              concurrent: false,
            });
          },
        },
      ],
      {rendererOptions: {collapse: false}}
    );
    await deleteTasks.run();
  } catch (e: unknown) {
    console.error(e);
  }
}
