export const prefixes = {
  PROD: /^Region|^Support|^Consensus/m,
  'PROD-2': /^PROD-2-/m,
  'PROD-3': /^PROD-3-/m,
  'PROD-4': /^PROD-4-/m,
  'PROD-5': /^PROD-5-/m,
  DEV: /^DEV-\D/m,
  'DEV-2': /^DEV-2-/m,
  HACK: /^HACK-\D/m,
};
