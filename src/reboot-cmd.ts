import {
  RebootInstancesCommand,
  DescribeNetworkInterfacesCommand,
  EC2Client,
  NetworkInterface,
} from '@aws-sdk/client-ec2';
import {Listr} from 'listr2';
import {regions} from './config';
import {
  populateStacksByRegion,
  stacksByRegion,
  Ctx,
  getInstanceIps,
  getInstanceIdsForStacks,
} from './utils';

export async function reboot(ips: string[]) {
  //get regions from ips
  const mainTasks = new Listr<Ctx>(
    [
      {
        title: 'Getting PROD/DEV net CloudFormation Stacks by region',
        task: (_ctx, task): Listr => {
          const tasks = Object.keys(regions).map(region => {
            return {
              title: region,
              task: () => populateStacksByRegion(region),
            };
          });

          return task.newListr(tasks, {
            concurrent: true,
          });
        },
      },
      {
        title: 'Getting EC2 instances allocated to each Stack',
        task: (ctx, task): Listr => {
          const regionTasks = Object.keys(stacksByRegion).map(region => {
            return {
              title: region,
              task: () =>
                Promise.all(
                  Object.keys(stacksByRegion[region]).map(stack => {
                    return getInstanceIdsForStacks(region, stack);
                  })
                ),
            };
          });
          return task.newListr(regionTasks, {concurrent: false});
        },
      },
      {
        title: 'Getting EC2 instance IPs',
        task: (ctx, task): Listr => {
          const regionTasks = Object.keys(stacksByRegion).map(region => {
            return {
              title: region,
              task: () => getInstanceIps(region),
            };
          });
          return task.newListr(regionTasks, {concurrent: false});
        },
      },
    ],
    {rendererOptions: {collapse: false}}
  );

  try {
    await mainTasks.run();
  } catch (e: unknown) {
    console.error(e);
  }

  console.log();
  console.log('============================');
  console.log('====== Rebooting Nodes =====');
  console.log('============================');
  console.log();

  const ipToRegionMap = getRegionsForIps(ips);
  for (const ip in ipToRegionMap) {
    getInstanceIdFromIp(ipToRegionMap[ip], ip).then(async instanceID => {
      if (instanceID) {
        await _reboot(ipToRegionMap[ip], [instanceID]);
      }
    });
  }
}

function getRegionsForIps(ips: string[]): {[ip: string]: string} {
  const dict: {[ip: string]: string} = {};
  for (const ip of ips) {
    for (const region in stacksByRegion) {
      for (const stack in stacksByRegion[region]) {
        if (stacksByRegion[region][stack].includes('validator ' + ip)) {
          dict[ip] = region;
        }
      }
    }
  }
  return dict;
}

async function getNetworkInterfaces(
  region: string,
  ip: string
): Promise<NetworkInterface[]> {
  const collected: NetworkInterface[] = [];
  const filter: {} = [{Name: 'ip-address', Values: [ip]}];
  let NextToken;
  let NetworkInterfaces: NetworkInterface[] | undefined;
  do {
    ({NetworkInterfaces, NextToken} = await new EC2Client({
      region,
    }).send(new DescribeNetworkInterfacesCommand(filter)));
    if (NetworkInterfaces) {
      collected.push(...NetworkInterfaces);
    }
  } while (NextToken);
  return collected;
}

async function getInstanceIdFromIp(
  region: string,
  ip: string
): Promise<string | undefined> {
  const instances = await getNetworkInterfaces(region, ip);
  for (const instance of instances) {
    if (instance.Association?.PublicIp === ip) {
      return instance.Attachment?.InstanceId;
    }
  }
  return undefined;
}

async function _reboot(region: string, InstanceIds: string[] | undefined) {
  await new EC2Client({
    region,
  }).send(
    new RebootInstancesCommand({
      InstanceIds,
    })
  );

  console.log('Rebooting ' + InstanceIds![0] + ' in ' + region);
}
