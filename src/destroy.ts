import {destroy} from './destroy-cmd';

function main() {
  const networkToDestroy = process.argv[2];
  destroy(networkToDestroy);
}

main();
