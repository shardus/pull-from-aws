import {
  CloudFormationClient,
  DeleteStackCommand,
  ListStackResourcesCommand,
  ListStacksCommand,
  StackResourceSummary,
  StackStatus,
  StackSummary,
} from '@aws-sdk/client-cloudformation';
import {
  DescribeInstancesCommand,
  EC2Client,
  Reservation,
} from '@aws-sdk/client-ec2';
import {prefixes} from './deploy-env-prefixes';

export async function listStacks(region: string): Promise<StackSummary[]> {
  const collected = [];
  let StackSummaries, NextToken;
  do {
    ({StackSummaries, NextToken} = await new CloudFormationClient({
      region,
    }).send(
      new ListStacksCommand({
        NextToken,
        StackStatusFilter: [
          StackStatus.CREATE_COMPLETE,
          StackStatus.CREATE_IN_PROGRESS,
        ],
      })
    ));
    if (StackSummaries) collected.push(...StackSummaries);
  } while (NextToken);
  return collected;
}

export async function listStackResources(
  region: string,
  StackName: string
): Promise<StackResourceSummary[]> {
  const collected = [];
  let StackResourceSummaries, NextToken;
  do {
    ({StackResourceSummaries, NextToken} = await new CloudFormationClient({
      region,
    }).send(
      new ListStackResourcesCommand({
        NextToken,
        StackName,
      })
    ));
    if (StackResourceSummaries) collected.push(...StackResourceSummaries);
  } while (NextToken);
  return collected;
}

export async function describeInstances(
  region: string,
  InstanceIds: string[]
): Promise<Reservation[]> {
  const collected = [];
  let Reservations, NextToken;
  do {
    ({Reservations, NextToken} = await new EC2Client({
      region,
    }).send(
      new DescribeInstancesCommand({
        NextToken,
        InstanceIds,
      })
    ));
    if (Reservations) collected.push(...Reservations);
  } while (NextToken);
  return collected;
}

export async function deleteStack(region: string, StackName: string) {
  await new CloudFormationClient({
    region,
  }).send(
    new DeleteStackCommand({
      StackName,
    })
  );
}

export function stacksByRegion2instancesByNetwork(stacksByRegion: {
  [region: string]: {
    [stack: string]: string[];
  };
}): {
  [network: string]: {
    [region: string]: {
      [stack: string]: string[];
    };
  };
} {
  const instancesByNetwork: {
    [network: string]: {[region: string]: {[stack: string]: string[]}};
  } = {};

  for (const prefix in prefixes) {
    instancesByNetwork[prefix] = {};
  }

  for (const region in stacksByRegion) {
    for (const stack in stacksByRegion[region]) {
      for (const prefix in prefixes) {
        const prefixRegex = prefixes[prefix as keyof typeof prefixes];
        if (prefixRegex.test(stack)) {
          if (!instancesByNetwork[prefix][region])
            instancesByNetwork[prefix][region] = {};
          instancesByNetwork[prefix][region][stack] =
            stacksByRegion[region][stack];
          break;
        }
      }
    }
  }

  return instancesByNetwork;
}

export interface Ctx {
  skip: boolean;
}

export const stacksByRegion: {[region: string]: {[stack: string]: string[]}} =
  {};

export async function populateStacksByRegion(region: string) {
  const stacks = await listStacks(region);
  for (const stack of stacks) {
    if (
      stack.StackName &&
      stack.StackId &&
      (stack.StackName.includes('SupportNodesStack') ||
        stack.StackName.includes('RegionSetupStack') ||
        stack.StackName.includes('ConsensusNodesStack'))
    ) {
      if (!stacksByRegion[region]) stacksByRegion[region] = {};
      stacksByRegion[region][stack.StackName] = [];
    }
  }
}

export async function getInstanceIdsForStacks(region: string, stack: string) {
  const resources = await listStackResources(region, stack);
  for (const resource of resources) {
    // Put them into stacksByRegion
    if (
      resource.ResourceType === 'AWS::EC2::Instance' &&
      resource.PhysicalResourceId
    ) {
      stacksByRegion[region][stack].push(resource.PhysicalResourceId);
    }
  }
}

export async function getInstanceIps(region: string) {
  // Collect instance IDs
  const instanceIds: string[] = [];
  for (const stack in stacksByRegion[region]) {
    instanceIds.push(...stacksByRegion[region][stack]);
  }

  // Get detailed info for instance IDs by region
  const reservations = await describeInstances(region, instanceIds);
  for (const reservation of reservations) {
    if (reservation.Instances) {
      // Match detailed info to each ID in stacksByRegion
      for (const instance of reservation.Instances) {
        for (const stack in stacksByRegion[region]) {
          for (const [idx, id] of stacksByRegion[region][stack].entries()) {
            if (id === instance.InstanceId && instance.PublicIpAddress) {
              // Put relevant data from detailed info into stacksByRegion
              const name = instance.Tags?.find(
                tag => tag.Key === 'Name'
              )?.Value;
              const ip = instance.PublicIpAddress;
              stacksByRegion[region][stack][idx] = `${name} ${ip}`;
            }
          }
        }
      }
    }
  }
}
