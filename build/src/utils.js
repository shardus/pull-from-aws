"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getInstanceIps = exports.getInstanceIdsForStacks = exports.populateStacksByRegion = exports.stacksByRegion = exports.stacksByRegion2instancesByNetwork = exports.deleteStack = exports.describeInstances = exports.listStackResources = exports.listStacks = void 0;
const client_cloudformation_1 = require("@aws-sdk/client-cloudformation");
const client_ec2_1 = require("@aws-sdk/client-ec2");
const deploy_env_prefixes_1 = require("./deploy-env-prefixes");
async function listStacks(region) {
    const collected = [];
    let StackSummaries, NextToken;
    do {
        ({ StackSummaries, NextToken } = await new client_cloudformation_1.CloudFormationClient({
            region,
        }).send(new client_cloudformation_1.ListStacksCommand({
            NextToken,
            StackStatusFilter: [
                client_cloudformation_1.StackStatus.CREATE_COMPLETE,
                client_cloudformation_1.StackStatus.CREATE_IN_PROGRESS,
            ],
        })));
        if (StackSummaries)
            collected.push(...StackSummaries);
    } while (NextToken);
    return collected;
}
exports.listStacks = listStacks;
async function listStackResources(region, StackName) {
    const collected = [];
    let StackResourceSummaries, NextToken;
    do {
        ({ StackResourceSummaries, NextToken } = await new client_cloudformation_1.CloudFormationClient({
            region,
        }).send(new client_cloudformation_1.ListStackResourcesCommand({
            NextToken,
            StackName,
        })));
        if (StackResourceSummaries)
            collected.push(...StackResourceSummaries);
    } while (NextToken);
    return collected;
}
exports.listStackResources = listStackResources;
async function describeInstances(region, InstanceIds) {
    const collected = [];
    let Reservations, NextToken;
    do {
        ({ Reservations, NextToken } = await new client_ec2_1.EC2Client({
            region,
        }).send(new client_ec2_1.DescribeInstancesCommand({
            NextToken,
            InstanceIds,
        })));
        if (Reservations)
            collected.push(...Reservations);
    } while (NextToken);
    return collected;
}
exports.describeInstances = describeInstances;
async function deleteStack(region, StackName) {
    await new client_cloudformation_1.CloudFormationClient({
        region,
    }).send(new client_cloudformation_1.DeleteStackCommand({
        StackName,
    }));
}
exports.deleteStack = deleteStack;
function stacksByRegion2instancesByNetwork(stacksByRegion) {
    const instancesByNetwork = {};
    for (const prefix in deploy_env_prefixes_1.prefixes) {
        instancesByNetwork[prefix] = {};
    }
    for (const region in stacksByRegion) {
        for (const stack in stacksByRegion[region]) {
            for (const prefix in deploy_env_prefixes_1.prefixes) {
                const prefixRegex = deploy_env_prefixes_1.prefixes[prefix];
                if (prefixRegex.test(stack)) {
                    if (!instancesByNetwork[prefix][region])
                        instancesByNetwork[prefix][region] = {};
                    instancesByNetwork[prefix][region][stack] =
                        stacksByRegion[region][stack];
                    break;
                }
            }
        }
    }
    return instancesByNetwork;
}
exports.stacksByRegion2instancesByNetwork = stacksByRegion2instancesByNetwork;
exports.stacksByRegion = {};
async function populateStacksByRegion(region) {
    const stacks = await listStacks(region);
    for (const stack of stacks) {
        if (stack.StackName &&
            stack.StackId &&
            (stack.StackName.includes('SupportNodesStack') ||
                stack.StackName.includes('RegionSetupStack') ||
                stack.StackName.includes('ConsensusNodesStack'))) {
            if (!exports.stacksByRegion[region])
                exports.stacksByRegion[region] = {};
            exports.stacksByRegion[region][stack.StackName] = [];
        }
    }
}
exports.populateStacksByRegion = populateStacksByRegion;
async function getInstanceIdsForStacks(region, stack) {
    const resources = await listStackResources(region, stack);
    for (const resource of resources) {
        // Put them into stacksByRegion
        if (resource.ResourceType === 'AWS::EC2::Instance' &&
            resource.PhysicalResourceId) {
            exports.stacksByRegion[region][stack].push(resource.PhysicalResourceId);
        }
    }
}
exports.getInstanceIdsForStacks = getInstanceIdsForStacks;
async function getInstanceIps(region) {
    var _a, _b;
    // Collect instance IDs
    const instanceIds = [];
    for (const stack in exports.stacksByRegion[region]) {
        instanceIds.push(...exports.stacksByRegion[region][stack]);
    }
    // Get detailed info for instance IDs by region
    const reservations = await describeInstances(region, instanceIds);
    for (const reservation of reservations) {
        if (reservation.Instances) {
            // Match detailed info to each ID in stacksByRegion
            for (const instance of reservation.Instances) {
                for (const stack in exports.stacksByRegion[region]) {
                    for (const [idx, id] of exports.stacksByRegion[region][stack].entries()) {
                        if (id === instance.InstanceId && instance.PublicIpAddress) {
                            // Put relevant data from detailed info into stacksByRegion
                            const name = (_b = (_a = instance.Tags) === null || _a === void 0 ? void 0 : _a.find(tag => tag.Key === 'Name')) === null || _b === void 0 ? void 0 : _b.Value;
                            const ip = instance.PublicIpAddress;
                            exports.stacksByRegion[region][stack][idx] = `${name} ${ip}`;
                        }
                    }
                }
            }
        }
    }
}
exports.getInstanceIps = getInstanceIps;
//# sourceMappingURL=utils.js.map