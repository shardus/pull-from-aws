export declare function deleteNetworkStacksInRegion(task: any, regionName: string, region: {
    [stack: string]: string[];
}): Promise<void>;
export declare function destroy(networkToDestroy: string): Promise<void>;
