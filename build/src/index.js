#!/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const listr2_1 = require("listr2");
const util_1 = require("util");
const config_1 = require("./config");
const destroy_cmd_1 = require("./destroy-cmd");
const utils_1 = require("./utils");
async function main() {
    if (process.argv[2] === 'destroy') {
        if (!process.argv[3]) {
            throw 'No network given to destroy';
        }
        (0, destroy_cmd_1.destroy)(process.argv[3]);
    }
    else {
        listNetworks();
    }
    return;
}
async function listNetworks() {
    const mainTasks = new listr2_1.Listr([
        {
            title: 'Getting PROD/DEV net CloudFormation Stacks by region',
            task: (_ctx, task) => {
                const tasks = Object.keys(config_1.regions).map(region => {
                    return {
                        title: region,
                        task: () => (0, utils_1.populateStacksByRegion)(region),
                    };
                });
                return task.newListr(tasks, {
                    concurrent: true,
                });
            },
        },
        {
            title: 'Getting EC2 instances allocated to each Stack',
            task: (ctx, task) => {
                const regionTasks = Object.keys(utils_1.stacksByRegion).map(region => {
                    return {
                        title: region,
                        task: () => Promise.all(Object.keys(utils_1.stacksByRegion[region]).map(stack => {
                            return (0, utils_1.getInstanceIdsForStacks)(region, stack);
                        })),
                    };
                });
                return task.newListr(regionTasks, { concurrent: false });
            },
        },
        {
            title: 'Getting EC2 instance IPs',
            task: (ctx, task) => {
                const regionTasks = Object.keys(utils_1.stacksByRegion).map(region => {
                    return {
                        title: region,
                        task: () => (0, utils_1.getInstanceIps)(region),
                    };
                });
                return task.newListr(regionTasks, { concurrent: false });
            },
        },
    ], { rendererOptions: { collapse: false } });
    try {
        await mainTasks.run();
        // Rearrange stacksByRegion into instancesByNetwork
        const instancesByNetwork = (0, utils_1.stacksByRegion2instancesByNetwork)(utils_1.stacksByRegion);
        if (process.argv[2] === '--json') {
            console.log(JSON.stringify(instancesByNetwork));
            console.log(JSON.stringify(utils_1.stacksByRegion));
        }
        else {
            console.log();
            console.log('============================');
            console.log('=== INSTANCES BY NETWORK ===');
            console.log('============================');
            console.log();
            console.log((0, util_1.inspect)(instancesByNetwork, false, null, true));
            console.log();
            console.log('========================');
            console.log('=== STACKS BY REGION ===');
            console.log('========================');
            console.log();
            console.log((0, util_1.inspect)(utils_1.stacksByRegion, false, null, true));
        }
    }
    catch (e) {
        console.error(e);
    }
}
main();
//# sourceMappingURL=index.js.map