"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const delay_1 = __importDefault(require("delay"));
/* eslint-disable @typescript-eslint/no-empty-function */
const listr2_1 = require("listr2");
const listr2_2 = require("listr2");
const logger = new listr2_2.Logger({ useIcons: false });
async function main() {
    let task;
    // JQYWVb3x1scokQK2IzhwA4F0qxWbYzXR
    logger.start('Example for subtasks.');
    task = new listr2_1.Listr([
        {
            title: 'This task will execute.',
            task: (_, task) => task.newListr([
                {
                    title: 'This is a subtask.',
                    task: async () => {
                        await (0, delay_1.default)(3000);
                    },
                },
            ]),
        },
    ], { concurrent: false });
    try {
        const context = await task.run();
        logger.success(`Context: ${JSON.stringify(context)}`);
    }
    catch (e) {
        logger.fail(e);
    }
    // 3ygUxrTkCDLDfDrXzF1ocWR6626jaL0kA
    // 8TNRrm8bI9ndz4jrhYC492luGNhtMTmZ
    logger.start('Example for subtasks with different renderer options.');
    task = new listr2_1.Listr([
        {
            title: 'This task will execute.',
            task: (_, task) => task.newListr([
                {
                    title: 'This is a subtask.',
                    task: async () => {
                        await (0, delay_1.default)(3000);
                    },
                },
                {
                    title: 'This is an another subtask.',
                    task: async () => {
                        await (0, delay_1.default)(2000);
                    },
                },
            ], { concurrent: true }),
        },
        {
            title: 'This task will execute.',
            task: (_, task) => task.newListr([
                {
                    title: 'This is a subtask.',
                    task: async () => {
                        await (0, delay_1.default)(3000);
                    },
                },
                {
                    title: 'This is an another subtask.',
                    task: async () => {
                        await (0, delay_1.default)(2000);
                    },
                },
            ], { concurrent: true, rendererOptions: { collapse: false } }),
        },
    ], { concurrent: false });
    try {
        const context = await task.run();
        logger.success(`Context: ${JSON.stringify(context)}`);
    }
    catch (e) {
        logger.fail(e);
    }
    logger.start('Example for subtasks with different disabled rendering from parent.');
    task = new listr2_1.Listr([
        {
            title: 'This task will execute but will not render subtasks.',
            task: (_, task) => task.newListr([
                {
                    title: 'This is a subtask.',
                    task: async () => {
                        await (0, delay_1.default)(3000);
                    },
                },
                {
                    title: 'This is an another subtask.',
                    task: async () => {
                        await (0, delay_1.default)(2000);
                    },
                },
            ], { concurrent: true }),
        },
    ], { concurrent: false, rendererOptions: { showSubtasks: false } });
    try {
        const context = await task.run();
        logger.success(`Context: ${JSON.stringify(context)}`);
    }
    catch (e) {
        logger.fail(e);
    }
    // 12ttDlnth5pr1YuJEnfXm3I2CzRbcFlY
    logger.start('Example for subtasks that change exit on error.');
    task = new listr2_1.Listr([
        {
            title: 'This task will execute and not quit on errors.',
            task: (_, task) => task.newListr([
                {
                    title: 'This is a subtask.',
                    task: async () => {
                        throw new Error('I have failed [0]');
                    },
                },
                {
                    title: 'This is an another subtask.',
                    task: async () => {
                        throw new Error('I have failed [1]');
                    },
                },
                {
                    title: 'This is yet an another subtask.',
                    task: async (_, task) => {
                        task.title = 'I have succeeded.';
                    },
                },
            ], { exitOnError: false }),
        },
        {
            title: 'This task will execute.',
            task: () => {
                throw new Error('I will exit on error since I am a direct child of parent task.');
            },
        },
    ], { concurrent: false, exitOnError: true });
    try {
        const context = await task.run();
        logger.success(`Context: ${JSON.stringify(context)}`);
    }
    catch (e) {
        logger.fail(e);
    }
    logger.data('You can also access all the errors spew out by the tasks by `task.err` which will return an array of errors.');
    logger.fail(task.err.toString());
}
void main();
//# sourceMappingURL=listr-test.js.map