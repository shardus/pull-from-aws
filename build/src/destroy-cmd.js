"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.destroy = exports.deleteNetworkStacksInRegion = void 0;
const listr2_1 = require("listr2");
const config_1 = require("./config");
const utils_1 = require("./utils");
async function deleteNetworkStacksInRegion(task, regionName, region) {
    const deleteCmds = Object.keys(region).map(stack => {
        task.output = `${regionName}, ${stack}`;
        return (0, utils_1.deleteStack)(regionName, stack);
    });
    await Promise.all(deleteCmds);
}
exports.deleteNetworkStacksInRegion = deleteNetworkStacksInRegion;
async function destroy(networkToDestroy) {
    try {
        // Get PROD/DEV network stacks
        const mainTasks = new listr2_1.Listr([
            {
                title: 'Getting PROD/DEV net CloudFormation Stacks by region',
                task: (_ctx, task) => {
                    const tasks = Object.keys(config_1.regions).map(region => {
                        return {
                            title: region,
                            task: () => (0, utils_1.populateStacksByRegion)(region),
                        };
                    });
                    return task.newListr(tasks, {
                        concurrent: true,
                    });
                },
            },
        ], { rendererOptions: { collapse: false } });
        await mainTasks.run();
        // Rearrange stacksByRegion into instancesByNetwork
        const instancesByNetwork = (0, utils_1.stacksByRegion2instancesByNetwork)(utils_1.stacksByRegion);
        /*
        const instancesByNetwork: {
          [network: string]: {[region: string]: {[stack: string]: string[]}};
        } = {
          PROD: {},
          'PROD-2': {},
          DEV: {},
        };
    
        for (const region in stacksByRegion) {
          for (const stack in stacksByRegion[region]) {
            switch (true) {
              case stack.includes('PROD-2'): {
                if (!instancesByNetwork['PROD-2'][region])
                  instancesByNetwork['PROD-2'][region] = {};
                instancesByNetwork['PROD-2'][region][stack] =
                  stacksByRegion[region][stack];
                break;
              }
              case stack.includes('DEV'): {
                if (!instancesByNetwork['DEV'][region])
                  instancesByNetwork['DEV'][region] = {};
                instancesByNetwork['DEV'][region][stack] =
                  stacksByRegion[region][stack];
                break;
              }
              default:
                if (!instancesByNetwork['PROD'][region])
                  instancesByNetwork['PROD'][region] = {};
                instancesByNetwork['PROD'][region][stack] =
                  stacksByRegion[region][stack];
                break;
            }
          }
        }
        */
        // Check if network is already down
        const network = instancesByNetwork[networkToDestroy];
        if (!network) {
            console.log('Network is not up');
            // eslint-disable-next-line no-process-exit
            process.exit();
        }
        // Delete stacks for the given Network
        const deleteTasks = new listr2_1.Listr([
            {
                title: `Deleting stacks for ${networkToDestroy} network`,
                task: (_ctx, task) => {
                    const tasks = Object.keys(instancesByNetwork[networkToDestroy]).map(region => {
                        return {
                            title: region,
                            task: () => deleteNetworkStacksInRegion(task, region, instancesByNetwork[networkToDestroy][region]),
                        };
                    });
                    return task.newListr(tasks, {
                        concurrent: false,
                    });
                },
            },
        ], { rendererOptions: { collapse: false } });
        await deleteTasks.run();
    }
    catch (e) {
        console.error(e);
    }
}
exports.destroy = destroy;
//# sourceMappingURL=destroy-cmd.js.map