import { StackResourceSummary, StackSummary } from '@aws-sdk/client-cloudformation';
import { Reservation } from '@aws-sdk/client-ec2';
export declare function listStacks(region: string): Promise<StackSummary[]>;
export declare function listStackResources(region: string, StackName: string): Promise<StackResourceSummary[]>;
export declare function describeInstances(region: string, InstanceIds: string[]): Promise<Reservation[]>;
export declare function deleteStack(region: string, StackName: string): Promise<void>;
export declare function stacksByRegion2instancesByNetwork(stacksByRegion: {
    [region: string]: {
        [stack: string]: string[];
    };
}): {
    [network: string]: {
        [region: string]: {
            [stack: string]: string[];
        };
    };
};
export interface Ctx {
    skip: boolean;
}
export declare const stacksByRegion: {
    [region: string]: {
        [stack: string]: string[];
    };
};
export declare function populateStacksByRegion(region: string): Promise<void>;
export declare function getInstanceIdsForStacks(region: string, stack: string): Promise<void>;
export declare function getInstanceIps(region: string): Promise<void>;
