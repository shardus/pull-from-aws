"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.reboot = void 0;
const client_ec2_1 = require("@aws-sdk/client-ec2");
const listr2_1 = require("listr2");
const config_1 = require("./config");
const utils_1 = require("./utils");
async function reboot(ips) {
    //get regions from ips
    const mainTasks = new listr2_1.Listr([
        {
            title: 'Getting PROD/DEV net CloudFormation Stacks by region',
            task: (_ctx, task) => {
                const tasks = Object.keys(config_1.regions).map(region => {
                    return {
                        title: region,
                        task: () => (0, utils_1.populateStacksByRegion)(region),
                    };
                });
                return task.newListr(tasks, {
                    concurrent: true,
                });
            },
        },
        {
            title: 'Getting EC2 instances allocated to each Stack',
            task: (ctx, task) => {
                const regionTasks = Object.keys(utils_1.stacksByRegion).map(region => {
                    return {
                        title: region,
                        task: () => Promise.all(Object.keys(utils_1.stacksByRegion[region]).map(stack => {
                            return (0, utils_1.getInstanceIdsForStacks)(region, stack);
                        })),
                    };
                });
                return task.newListr(regionTasks, { concurrent: false });
            },
        },
        {
            title: 'Getting EC2 instance IPs',
            task: (ctx, task) => {
                const regionTasks = Object.keys(utils_1.stacksByRegion).map(region => {
                    return {
                        title: region,
                        task: () => (0, utils_1.getInstanceIps)(region),
                    };
                });
                return task.newListr(regionTasks, { concurrent: false });
            },
        },
    ], { rendererOptions: { collapse: false } });
    try {
        await mainTasks.run();
    }
    catch (e) {
        console.error(e);
    }
    console.log();
    console.log('============================');
    console.log('====== Rebooting Nodes =====');
    console.log('============================');
    console.log();
    const ipToRegionMap = getRegionsForIps(ips);
    for (const ip in ipToRegionMap) {
        getInstanceIdFromIp(ipToRegionMap[ip], ip).then(async (instanceID) => {
            if (instanceID) {
                await _reboot(ipToRegionMap[ip], [instanceID]);
            }
        });
    }
}
exports.reboot = reboot;
function getRegionsForIps(ips) {
    const dict = {};
    for (const ip of ips) {
        for (const region in utils_1.stacksByRegion) {
            for (const stack in utils_1.stacksByRegion[region]) {
                if (utils_1.stacksByRegion[region][stack].includes('validator ' + ip)) {
                    dict[ip] = region;
                }
            }
        }
    }
    return dict;
}
async function getNetworkInterfaces(region, ip) {
    const collected = [];
    const filter = [{ Name: 'ip-address', Values: [ip] }];
    let NextToken;
    let NetworkInterfaces;
    do {
        ({ NetworkInterfaces, NextToken } = await new client_ec2_1.EC2Client({
            region,
        }).send(new client_ec2_1.DescribeNetworkInterfacesCommand(filter)));
        if (NetworkInterfaces) {
            collected.push(...NetworkInterfaces);
        }
    } while (NextToken);
    return collected;
}
async function getInstanceIdFromIp(region, ip) {
    var _a, _b;
    const instances = await getNetworkInterfaces(region, ip);
    for (const instance of instances) {
        if (((_a = instance.Association) === null || _a === void 0 ? void 0 : _a.PublicIp) === ip) {
            return (_b = instance.Attachment) === null || _b === void 0 ? void 0 : _b.InstanceId;
        }
    }
    return undefined;
}
async function _reboot(region, InstanceIds) {
    await new client_ec2_1.EC2Client({
        region,
    }).send(new client_ec2_1.RebootInstancesCommand({
        InstanceIds,
    }));
    console.log('Rebooting ' + InstanceIds[0] + ' in ' + region);
}
//# sourceMappingURL=reboot-cmd.js.map