export declare const regions: {
    'us-east-1': number;
    'ap-east-1': number;
    'eu-central-1': number;
    'eu-north-1': number;
    'eu-south-1': number;
    'eu-west-1': number;
    'eu-west-3': number;
    'sa-east-1': number;
    'us-west-2': number;
};
