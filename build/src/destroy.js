"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const destroy_cmd_1 = require("./destroy-cmd");
function main() {
    const networkToDestroy = process.argv[2];
    (0, destroy_cmd_1.destroy)(networkToDestroy);
}
main();
//# sourceMappingURL=destroy.js.map