"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const reboot_cmd_1 = require("./reboot-cmd");
function main() {
    const nodesToReboot = process.argv.slice(2);
    (0, reboot_cmd_1.reboot)(nodesToReboot);
}
main();
//# sourceMappingURL=reboot.js.map