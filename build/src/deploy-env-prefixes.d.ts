export declare const prefixes: {
    PROD: RegExp;
    'PROD-2': RegExp;
    'PROD-3': RegExp;
    'PROD-4': RegExp;
    'PROD-5': RegExp;
    DEV: RegExp;
    'DEV-2': RegExp;
    HACK: RegExp;
};
