# pull-from-aws

A CLI tool to pull info about the EC2 instances that form the Shardeum networks from AWS.

It uses the [AWS CLI](https://aws.amazon.com/cli/) to list the [regions](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-regions), [stack names](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/stacks.html), and IP addresses of all EC2 instances deployed on AWS for each Shardeum network (`PROD`, `PROD-2`, and `DEV`).

![pull-from-aws demo](./pull-from-aws-demo.gif)

## Installation and Usage

There are two ways to install `pull-from-aws`:

### Dev Container Project

1. Follow these steps to install Docker, VSCode, and the Remote Development extension pack: https://code.visualstudio.com/docs/remote/containers#_installation

2. Clone this repository and open it in VSCode. For Windows users, clone this repository into WSL2 for far better performance: https://docs.docker.com/desktop/windows/wsl/#best-practices 

3. Create a `.secrets` file in the project root that contains the following:

   ```bash
   # AWS CLI credentials passed to dev container as environment variables
   AWS_ACCESS_KEY_ID=<YOUR_ACCESS_KEY_HERE>
   AWS_SECRET_ACCESS_KEY=<YOUR_SECRET_ACCESS_KEY_HERE>
   AWS_DEFAULT_REGION=eu-north-1
   ```

4. Open the Command Palette (`Shift`+`Command`+`P` (Mac) / `Ctrl`+`Shift`+`P` (Windows/Linux)) then type and select the `Remote-Containers: Rebuild and Reopen in Container` option. This will automatically pull the [Shardus dev-container](https://gitlab.com/shardus/dev-container) and install the AWS CLI tool inside of it.

5. Open a terminal in VSCode (`Command`+`~` (Mac)/`Ctrl`+`~` (Windows/Linux)) and run:

   ```bash
   npm install
   npm run pull
   ```
   To output in JSON format, run

   ```bash
   npm run pull -- --json
   ```


6. To destroy a network, run:

   ```bash
   npm run destroy <NETWORK>
   ```

7. To reboot a node or set of nodes, run:

   ```bash
   npm run reboot <ip> <ip> <ip> ...
   ```

### NPM Global Package

1. Ensure Node.js and NPM are installed on your system.

2. Install the AWS CLI tool: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html#getting-started-install-instructions

3. Make sure you have AWS credentials configured in your home directory as described here: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html

4. Use NPM to install this package globally with:

   ```bash
   npm install -g https://gitlab.com/shardus/pull-from-aws.git
   ```

5. Open a terminal and run the following command:

   ```bash
   $ pull-from-aws
   ```

6. To destroy a network, run:

   ```bash
   $ pull-from-aws destroy <NETWORK>
   ```
